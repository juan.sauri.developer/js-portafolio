import React from "react"; // importamos la libreria react
import Verde from "../Verde/Verde";// importamos el componente verde poniendo su direccion
import "../estilos/estilos.css"// importamos los estilos del archivo en css poniendo su direccion de ubicacion
import { useState } from 'react'; // importamos el hook useState de la libreria de react

function Main (){ //creamos la funcion main sin darle parametros
    const [verde_o_azul, cambiar_color] = useState(true); // estamos creando y asignando dos variables donde se guardan los dos primeros elementos de lo que devuelve la funcion use state
    const cambioComponente = () => {  
        cambiar_color(!verde_o_azul);
        
    };// creamos la variable cambioComponente y guardamos dentro una funcion que usa como parametro la primera variable que devuelve la funcion use state y la niega
    return( 
    <div>
        <div>   
        <button onClick={cambioComponente}>Azul/Verde</button>
        {verde_o_azul && <Verde/>}
        </div>
        
    </div>// en esta linea termina el return que es la salida de la funcion main donde se usa un boton al cual se le asigna la funcion onClick para ejecutar una condicion y mostrar el componente verde
    
    );
}
export default Main; // se exporta el componente main para ser usado en index.js