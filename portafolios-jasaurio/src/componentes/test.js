'use strict'
function a(){   // creamos la funcion a

    const arr = []; //declaramos la variable arr y le asignamos un arreglo vacío
    arr.push("masculino"); // aplicamos el metodo push para arreglos al arreglo arr agregando un elemento tipo string llamado masculino 
    arr.push("juan"); // aplicamos el metodo push para arreglos al arreglo arr agregando un elemento tipo string llamado juan
    
    return arr; // la funcion devuelve el arreglo con los nuevos valores
};


const data = a(); // creamos una variable de nombre data en el que se asigna el resultado de la funcion a que es un arreglo 
const nombreIndividual= data[0]; // creamos y asignamos una variable con el primer elemento del arreglo data
const generoIndividual= data[1]; // creamos y asignamos una variable con el segundo elemento del arreglo data
const [nombre,genero] = a(); // deconstruccion es la linea 15, equivale a las lineas 12 a la 14, crea y asigna una variable por cada elemento del arreglo resultado de la funcion a
console.log(1,a()); // usamos el metodo log de la consola para que muestre el resultado de la funcion a y le asignamos un numero para darle orden
console.log(2,nombre);// usamos el metodo log de la consola para que muestre el valor de la variable nombre y le asignamos un numero para darle orden
console.log(3,genero);// usamos el metodo log de la consola para que muestre el valor de la variable genero y le asignamos un numero para darle orden
console.log(4,data);// usamos el metodo log de la consola para que muestre el valor de la variable data y le asignamos un numero para darle orden
console.log(5,nombreIndividual);// usamos el metodo log de la consola para que muestre el valor de la variable nombreIndividual y le asignamos un numero para darle orden
console.log(6,generoIndividual);// usamos el metodo log de la consola para que muestre el valor de la variable generoIndividual y le asignamos un numero para darle orden
const func = a; // creamos la variable func y le asignamos el nombre de la funcion a
console.log("variableFunc",func); // usamos el metodo log de la consola para que muestre el string "variableFunc" y el contenido de la variable func
console.log("ejecucionDeFunc",func());// usamos el metodo log de la consola para que muestre el resultado de la funcion almacenada en la variable func